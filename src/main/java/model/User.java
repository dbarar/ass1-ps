package model;

import model.enumeration.UserRole;

public class User{
    private Long id;
    private UserRole role;
    private String username;
    private String password;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String usernama) {
        this.username = usernama;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return  " User: " +
                " id: " + getId() +
                " role: " + getRole() +
                " username: " + getUsername() +
                " name: " + getName();
    }

    @Override
    public boolean equals(Object o){
        if (o == this){
            return true;
        }
        if(!(o instanceof User)){
            return false;
        }

        return Long.compare(id, ((User) o).getId()) == 0;
    }
}

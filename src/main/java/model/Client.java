package model;


import java.util.List;

public class Client {
    private Long id;
    private String name;
    private Long personalNumericalCode;
    private int identityCardNumber;
    private String address;
    private List<Account> accounts;


    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPersonalNumericalCode() {
        return personalNumericalCode;
    }

    public void setPersonalNumericalCode(Long personalNumericalCode) {
        this.personalNumericalCode = personalNumericalCode;
    }

    public int getIdentityCardNumber() {
        return identityCardNumber;
    }

    public void setIdentityCardNumber(int identityCardNumber) {
        this.identityCardNumber = identityCardNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o){
        if (o == this){
            return true;
        }
        if(!(o instanceof Client)){
            return false;
        }

        return Long.compare(id, ((Client) o).getId()) == 0;
    }

    @Override
    public String toString(){
        return "Client:" +
                " id: " + id +
                " name: " + name +
                " personal numerical code: " + personalNumericalCode +
                " identity card number: " + identityCardNumber +
                " address: " + address;
    }
}

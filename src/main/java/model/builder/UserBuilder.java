package model.builder;

import model.User;
import model.enumeration.UserRole;

public class UserBuilder {
    private Long id;
    private String username = "defaultUsername";
    private String password = "defaultPass";
    private String name = "defaultName";
    private UserRole role = UserRole.USER;

    public UserBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public UserBuilder withUsername (String username) {
        this.username = username;
        return this;
    }

    public UserBuilder withPassword (String password) {
        this.password = password;
        return this;
    }

    public UserBuilder withName (String name) {
        this.name = name;
        return this;
    }

    public UserBuilder withUserRole (UserRole userRole) {
        this.role = userRole;
        return this;
    }

    public User build(){
        User user = new User();

        user.setId(id);
        user.setName(name);
        user.setUsername(username);
        user.setPassword(password);
        user.setRole(role);

        return user;
    }
}

package model.builder;

import model.Account;
import model.Client;
import model.enumeration.AccountType;

import java.sql.Date;

public class AccountBuilder {
    private Long id;
    private Double balance = 0.0;
    private boolean active = true;
    private Client client;
    private AccountType type = AccountType.CREDIT;
    private Date creationDate;
    private Long iban;

    public AccountBuilder withIban(Long iban){
        this.iban = iban;
        return this;
    }

    public AccountBuilder withCreationDate(Date date){
        this.creationDate = date;
        return this;
    }

    public AccountBuilder withType(AccountType type){
        this.type = type;
        return this;
    }

    public AccountBuilder withId(Long id){
        this.id = id;
        return this;
    }

    public AccountBuilder withBalance(Double balance){
        this.balance = balance;
        return this;
    }

    public AccountBuilder withActive(boolean active){
        this.active = active;
        return this;
    }

    public AccountBuilder withClient(Client client){
        this.client = client;
        return this;
    }

    public Account build(){
        Account account = new Account();

        account.setId(id);
        account.setBalance(balance);
        account.setActive(active);
        account.setClient(client);
        account.setType(type);
        account.setCreationDate(creationDate);
        account.setIban(iban);

        return account;
    }
}

package model.builder;

import model.Account;
import model.Client;
import org.omg.PortableInterceptor.ACTIVE;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class ClientBuilder {
    private Long id;
    private String name = "DefaultName";
    private Long personalNumericalCode = 2960901303910L;
    private int identityCardNumber = 602794;
    private String address = "Default Address";
    private List<Account> accounts = new ArrayList<>();


    public ClientBuilder withId(Long id){
        this.id = id;
        return this;
    }

    public ClientBuilder withAccounts(List<Account> accounts){
        this.accounts = accounts;
        return this;
    }

    public ClientBuilder withName(String name){
        this.name = name;
        return this;
    }

    public ClientBuilder withPersonalNumericalCode(Long personalNumericalCode){
        this.personalNumericalCode = personalNumericalCode;
        return this;
    }

    public ClientBuilder withIdentityCardNumber(int identityCardNumber){
        this.identityCardNumber = identityCardNumber;
        return this;
    }

    public ClientBuilder withAddress(String address){
        this.address = address;
        return this;
    }

    public Client build(){
        Client client = new Client();

        client.setId(id);
        client.setName(name);
        client.setIdentityCardNumber(identityCardNumber);
        client.setPersonalNumericalCode(personalNumericalCode);
        client.setAddress(address);
        client.setAccounts(accounts);

        return client;
    }
}

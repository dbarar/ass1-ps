package model.enumeration;

public enum AccountType {
    CREDIT,
    DEBIT
}

package model;

import model.enumeration.AccountType;

import java.sql.Date;

public class Account {
    private Long id;
    private Double balance;
    private boolean active;
    private Client client;
    private AccountType type;
    private Date creationDate;
    private Long iban;

    public Long getIban() {
        return iban;
    }

    public void setIban(Long iban) {
        this.iban = iban;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public AccountType getType() {
        return type;
    }

    public void setType(AccountType type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o){
        if (o == this){
            return true;
        }
        if(!(o instanceof Account)){
            return false;
        }

        return Long.compare(id, ((Account) o).getId()) == 0;
    }

    @Override
    public String toString(){
        return "Account:" +
                " id: " + id +
                " balance: " + balance +
                " active: " + active +
                " client: " + client +
                " type: " + type.name() +
                " creationDate: " + creationDate +
                " iban: " + iban +
                "\n";
    }
}

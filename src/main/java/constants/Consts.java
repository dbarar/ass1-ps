package constants;

import java.util.logging.Logger;

public final class Consts {

    public static final String SCHEMA_NAME = "bank";

    public static final Logger logger = Logger.getLogger(Consts.class.getName());

    private Consts(){
        throw new AssertionError();
    }
}

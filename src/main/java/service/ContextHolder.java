package service;

import model.User;

public interface ContextHolder {

    User getLoggedInUser();

    void setLoggedInUser(User user);

    void logout();
}

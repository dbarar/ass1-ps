package service;

import model.User;

public interface UserService {

    User login(String userName, String password);

    void logout();

}

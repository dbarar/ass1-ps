package service;

import model.Account;
import model.Client;
import model.enumeration.AccountType;

import java.sql.Date;
import java.util.List;

public interface RegularUserService extends UserService {

    //CRUD on Client

    Client createClient(String name, Long personalNumericalCode, int identityCardNumber, String address);

    Client findClientById(Long id);

    Client findClientByCNP(Long cnp);

    Client updateClient(Long id, String name, Long personalNumericalCode, int identityCardNumber, String address);

    Boolean deleteClient(Long id);

    //CRUD on Account

    Account createAccount(AccountType accountType, Double balance, Date creationDate, Boolean active, Long clientId, Long iban);

    Account findAccountByIban(Long iban);

    Account findAccountById(Long id);

    Account updateAccount(Long id, AccountType accountType, Double balance, Date creationDate, Boolean active, Long clientId, Long iban);

    Boolean deleteAccount(Long id);

    //CURD on Account given Client

    List<Account> findAccountsByClient(Long clientID);

    //transfer between accounts

    Boolean transfer(Account account1, Account account2, Double amount);

    //process utility bills

}

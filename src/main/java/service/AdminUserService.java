package service;

import model.User;
import model.enumeration.UserRole;

public interface AdminUserService extends UserService {

    public User createUser(UserRole role, String username, String password, String name);

    public User findUserById(Long id);

    public User updateUser(Long id, UserRole role, String username, String password, String name);

    public boolean deleteUser(Long id);

    public User findUserByUsername(String userName);

}

package service.impl;

import model.User;
import repository.UserRep;
import service.ContextHolder;
import service.UserService;

import java.util.logging.Logger;

public class UserServiceImpl implements UserService {

    protected final UserRep userRepository;
    private final ContextHolder contextHolder;
    protected final Logger logger;

    public UserServiceImpl(UserRep userRepository, ContextHolder contextHolder, Logger logger) {
        this.userRepository = userRepository;
        this.contextHolder = contextHolder;
        this.logger = logger;
    }

    @Override
    public User login(String userName, String password) {
        User user = userRepository.loadByUserName(userName);
        if(user != null) {
            if(user.getPassword().equals(password)) {
                contextHolder.setLoggedInUser(user);
                logger.info("User just got logged in: " + user);
                return user;
            } else {
                logger.warning("Wrong password for user " + userName);
            }
        } else {
            logger.warning("User with username: " + userName + " was not found");
        }
        return null;
    }

    @Override
    public void logout() {
        contextHolder.setLoggedInUser(null);
        logger.info("User logged out");
    }
}

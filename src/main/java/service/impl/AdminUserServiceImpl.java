package service.impl;

import model.User;
import model.builder.UserBuilder;
import model.enumeration.UserRole;
import repository.UserRep;
import service.AdminUserService;
import service.ContextHolder;

import java.util.logging.Logger;

public class AdminUserServiceImpl extends UserServiceImpl implements AdminUserService {

    public AdminUserServiceImpl(UserRep userRepository, ContextHolder contextHolder, Logger logger){
        super(userRepository, contextHolder, logger);
    }

    @Override
    public User createUser(UserRole role, String username, String password, String name) {
        return userRepository.create(role, username, password, name);
    }

    @Override
    public User findUserById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public User updateUser(Long id, UserRole role, String username, String password, String name) {
        User user = new UserBuilder()
                .withId(id)
                .withUserRole(role)
                .withUsername(username)
                .withPassword(password)
                .withName(name)
                .build();
        return userRepository.update(user);
    }

    @Override
    public boolean deleteUser(Long id) {
        return userRepository.delete(id);
    }

    @Override
    public User findUserByUsername(String userName) {
        return userRepository.loadByUserName(userName);
    }
}

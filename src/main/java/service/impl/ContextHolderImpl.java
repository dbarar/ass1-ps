package service.impl;

import model.User;
import service.ContextHolder;

public class ContextHolderImpl implements ContextHolder{

    private static User currentUser;

    public static User getCurrentUser() {
        return currentUser;
    }

    @Override
    public User getLoggedInUser() {
        return currentUser;
    }

    @Override
    public void setLoggedInUser(User user) {
        currentUser = user;
    }

    @Override
    public void logout() {
        currentUser = null;
    }
}

package service.impl;

import constants.Consts;
import model.Account;
import model.Client;
import model.builder.AccountBuilder;
import model.builder.ClientBuilder;
import model.enumeration.AccountType;
import repository.AccountRep;
import repository.ClientRep;
import repository.ReportRep;
import repository.UserRep;
import service.ContextHolder;
import service.RegularUserService;
import utils.BillPrinter;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

public class RegularUserServiceImpl extends UserServiceImpl implements RegularUserService{

    private final AccountRep accountRep;
    private final ClientRep clientRep;
    private final ReportRep reportRep;
    private final ContextHolder contextHolder;
    private String fileName;
    private String toWrite;

    public RegularUserServiceImpl(UserRep userRepository, ContextHolder contextHolder, Logger logger, AccountRep accountRep, ClientRep clientRep, ReportRep reportRep){
        super(userRepository, contextHolder, logger);
        this.accountRep = accountRep;
        this.clientRep = clientRep;
        this.reportRep = reportRep;
        this.contextHolder = contextHolder;
    }

    @Override
    public Client createClient(String name, Long personalNumericalCode, int identityCardNumber, String address) {
        Client client = clientRep.create(name, personalNumericalCode, identityCardNumber, address);
        fileName = "CreateClient" + client.getId() + ".txt";
        toWrite = "Client created: " + client;
        BillPrinter.printBill(fileName, toWrite);
        reportRep.insertReport(contextHolder.getLoggedInUser().getId(), "Created a client " + client, new Date(Calendar.getInstance().getTimeInMillis()));
        return client;
    }

    @Override
    public Client findClientById(Long id) {
        reportRep.insertReport(contextHolder.getLoggedInUser().getId(), "Finding a client by id " + id, new Date(Calendar.getInstance().getTime().getTime()));
        return clientRep.findById(id);
    }

    @Override
    public Client findClientByCNP(Long cnp) {
        reportRep.insertReport(contextHolder.getLoggedInUser().getId(), "Finding a client by cnp " + cnp, new Date(Calendar.getInstance().getTimeInMillis()));
        return clientRep.findByCNP(cnp);
    }

    @Override
    public Client updateClient(Long id, String name, Long personalNumericalCode, int identityCardNumber, String address) {
        Client client = new ClientBuilder()
                .withId(id)
                .withName(name)
                .withPersonalNumericalCode(personalNumericalCode)
                .withIdentityCardNumber(identityCardNumber)
                .withAddress(address)
                .build();
        fileName = "UpdateClient" + client.getId() + ".txt";
        toWrite = "Client updated: " + client;
        BillPrinter.printBill(fileName, toWrite);
        reportRep.insertReport(contextHolder.getLoggedInUser().getId(), "Updating a client " + client, new Date(Calendar.getInstance().getTimeInMillis()));
        return clientRep.update(client);
    }

    @Override
    public Boolean deleteClient(Long id) {
        Client client = clientRep.findById(id);
        List<Account> accounts = accountRep.findByClient(client);
        if (accounts != null)
            for (Account account : accounts)
                accountRep.delete(account.getId());
        fileName = "DeleteClient" + client.getId() + ".txt";
        toWrite = "Client deleted: " + client;
        BillPrinter.printBill(fileName, toWrite);
        reportRep.insertReport(contextHolder.getLoggedInUser().getId(), "Deleting a client " + client, new Date(Calendar.getInstance().getTimeInMillis()));
        return clientRep.delete(id);
    }

    @Override
    public Account createAccount(AccountType accountType, Double balance, Date creationDate, Boolean active, Long clientId, Long iban) {
        Client client = clientRep.findById(clientId);
        Account account = accountRep.create(accountType, balance, creationDate, active, client, iban);

        client.getAccounts().add(account);
        fileName = "CreateAccount" + account.getId() + ".txt";
        toWrite = "Account created: " + account;
        BillPrinter.printBill(fileName, toWrite);
        reportRep.insertReport(contextHolder.getLoggedInUser().getId(), "Creating an account " + account, new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
        return account;
    }

    @Override
    public Account findAccountByIban(Long iban) {
        reportRep.insertReport(contextHolder.getLoggedInUser().getId(), "Finding an account by iban " + iban, new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
        return accountRep.findByIban(iban);
    }

    @Override
    public Account findAccountById(Long id) {
        reportRep.insertReport(contextHolder.getLoggedInUser().getId(), "Finding an account by id " + id, new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
        return accountRep.findById(id);
    }

    @Override
    public Account updateAccount(Long id, AccountType accountType, Double balance, Date creationDate, Boolean active, Long clientId, Long iban) {
        Client client = clientRep.findById(clientId);
        Account account = new AccountBuilder()
                .withId(id)
                .withBalance(balance)
                .withActive(active)
                .withClient(client)
                .withCreationDate(creationDate)
                .withType(accountType)
                .withIban(iban)
                .build();
        fileName = "UpdateAccount" + account.getId() + ".txt";
        toWrite = "Account updated: " + account;
        BillPrinter.printBill(fileName, toWrite);
        reportRep.insertReport(contextHolder.getLoggedInUser().getId(), "Updating an account " + account, new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
        return accountRep.update(account);
    }

    @Override
    public Boolean deleteAccount(Long id) {
        Account account = accountRep.findById(id);
        Client client = account.getClient();
        List<Account> accounts = client.getAccounts();

        accounts.remove(account);
        client.setAccounts(accounts);

        fileName = "DeleteAccount" + account.getId() + ".txt";
        toWrite = "Account deleted: " + account;
        BillPrinter.printBill(fileName, toWrite);
        reportRep.insertReport(contextHolder.getLoggedInUser().getId(), "Deleting an account " + account, new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
        return accountRep.delete(id);
    }

    @Override
    public List<Account> findAccountsByClient(Long clientID) {
        reportRep.insertReport(contextHolder.getLoggedInUser().getId(), "Finding accounts by client with id " + clientID, new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
        return accountRep.findByClient(clientRep.findById(clientID));
    }

    @Override
    public Boolean transfer(Account account1, Account account2, Double amount) {
        Double balance1 = account1.getBalance();
        Double balance2 = account2.getBalance();
        Boolean result = false;
        if (balance1 > amount){
            account1.setBalance(balance1 - amount);
            account2.setBalance(balance2 + amount);
            logger.info("Transfer from account1: " + account1 + "\nto account2: " + account2);
            accountRep.update(account1);
            accountRep.update(account2);
            result = true;
        }else{
            logger.info("The transfer from account1: " + account1 + "\nto account2: " + account2 + "\ncould not took place");
            account1.setBalance(balance1);
            account2.setBalance(balance2);
        }
        fileName = "Transfer" + account1.getId() + account2.getId() + amount.hashCode() + ".txt";
        toWrite = "Transfer from: " + account1 + "\nto" + account2;
        BillPrinter.printBill(fileName, toWrite);
        reportRep.insertReport(contextHolder.getLoggedInUser().getId(), "Transfer " + amount + " from " + account1 + " to " + account2, new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
        return result;
    }
}

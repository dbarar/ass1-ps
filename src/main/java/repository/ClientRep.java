package repository;

import model.Client;

import java.util.List;

public interface ClientRep {

    Client create(String name, Long personalNumericalCode, int identityCardNumber, String address);

    Client findById(Long id);

    Client findByCNP(Long cnp);

    List<Client> findAll();

    Client update(Client client);

    boolean delete(Long id);

    int countUsers();
}

package repository.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBConnectionWrapper {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/";
    private static final String USER = "root";
    private static final String PASS = "root";
    private static final int TIMEOUT = 5;

    private Connection connection;

    public JDBConnectionWrapper(String schemaName) {
        try {
            connection = DriverManager.getConnection(DB_URL + schemaName + "?useSSL=false", USER, PASS);
            createTables();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void createTables() throws SQLException {
        Statement statement = connection.createStatement();

        String sql = "CREATE TABLE IF NOT EXISTS user (" +
                "  id BIGINT(100) NOT NULL AUTO_INCREMENT," +
                "  role varchar(255) NOT NULL," +
                "  username varchar(255) NOT NULL," +
                "  password varchar(255) NOT NULL," +
                "  name varchar(255) NOT NULL," +
                "  PRIMARY KEY (id)," +
                "  UNIQUE KEY id_UNIQUE (id)" +
                ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";
        statement.execute(sql);

        sql = "CREATE TABLE IF NOT EXISTS client (" +
                "  id BIGINT(100) NOT NULL AUTO_INCREMENT," +
                "  name varchar(255) NOT NULL," +
                "  personal_numerical_code BIGINT(13) NOT NULL," +
                "  identity_card_number int(11) NOT NULL," +
                "  address varchar(45)," +
                "  PRIMARY KEY (id)," +
                "  UNIQUE KEY id_UNIQUE (id)" +
                ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";
        statement.execute(sql);

        sql = "CREATE TABLE IF NOT EXISTS account (" +
                "  id BIGINT(100) NOT NULL AUTO_INCREMENT," +
                "  user_id BIGINT(100) NOT NULL," +
                "  active TINYINT(1) NOT NULL," +
                "  balance  DOUBLE NOT NULL," +
                "  PRIMARY KEY (id)," +
                "  UNIQUE KEY id_UNIQUE (id)" +
                ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";
        statement.execute(sql);
    }

    public boolean testConnection() throws SQLException {
//        return connection.isValid(TIMEOUT);
          return connection.isReadOnly();
    }

    public Connection getConnection() {
        return connection;
    }
}

//    CREATE TABLE `bank`.`client_report` (
//        `idclient_report` BIGINT(100) NOT NULL,
//        `client_id` BIGINT(100) NOT NULL,
//        `report` VARCHAR(45) NULL,
//        `date` DATE NOT NULL,
//        PRIMARY KEY (`idclient_report`))
//        COMMENT = '		';
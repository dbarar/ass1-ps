package repository.impl;

import model.Account;
import model.Client;
import model.builder.AccountBuilder;
import model.enumeration.AccountType;
import repository.AccountRep;
import repository.ClientRep;

import java.sql.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class AccountRepImpl implements AccountRep {
    private final JDBConnectionWrapper jdbConnectionWrapper;

    private final Logger logger;

    private Connection connection;
    private String sqlQuery;
    private ResultSet resultSet;
    private PreparedStatement preparedStatement;
    private Account account;
    private List<Account> accounts;
    private ClientRep clientRep;

    public AccountRepImpl(JDBConnectionWrapper jdbConnectionWrapper, Logger logger, ClientRep clientRep) {
        this.jdbConnectionWrapper = jdbConnectionWrapper;
        this.logger = logger;
        this.clientRep = clientRep;
    }

    @Override
    public Account create(AccountType accountType, Double balance, Date creationDate, Boolean active, Client client, Long iban) {
        sqlQuery = "INSERT INTO bank.account (balance, active, user_id, creation_date, type, iban) " +
                "VALUES (?, ?, ?, ?, ?, ?)";
        account = new AccountBuilder()
                .withBalance(balance)
                .withActive(active)
                .withClient(client)
                .withCreationDate(creationDate)
                .withType(accountType)
                .withIban(iban)
                .build();

        connection = jdbConnectionWrapper.getConnection();
        try{
            preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setDouble(1, balance);
            preparedStatement.setBoolean(2, active);
            preparedStatement.setLong(3, client.getId());
            preparedStatement.setDate(4, creationDate);
            preparedStatement.setString(5,accountType.name());
            preparedStatement.setLong(6, iban);

            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();

            if(resultSet.next())
                account.setId(resultSet.getLong(1));
            resultSet.close();

            logger.info("Created account:" + account);
        }catch (SQLException e){
            e.printStackTrace();
            logger.info("While creating account, the following exception appeared" + e);
            account = null;
        }

        return account;
    }

    @Override
    public Account findByIban(Long iban) {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "SELECT * FROM bank.account WHERE iban=?";
        account = null;

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setLong(1, iban);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                account = new AccountBuilder()
                        .withId(resultSet.getLong(1))
                        .withBalance(resultSet.getDouble(2))
                        .withActive(resultSet.getBoolean(3))
                        .withClient(clientRep.findById(resultSet.getLong(4)))
                        .withCreationDate(resultSet.getDate(5))
                        .withType(AccountType.valueOf(resultSet.getString(6)))
                        .withIban(resultSet.getLong(7))
                        .build();
                logger.info("Account found by iban: " + account);
                resultSet.close();
            }
            else{
                logger.info("Account was not found by iban: " + iban);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("While finding the account by iban, the following exception appeared " + e);
        }

        return account;
    }

    @Override
    public Account findById(Long id) {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "SELECT * FROM bank.account WHERE id=?";
        account = null;

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                account = new AccountBuilder()
                        .withId(resultSet.getLong(1))
                        .withBalance(resultSet.getDouble(2))
                        .withActive(resultSet.getBoolean(3))
                        .withClient(clientRep.findById(resultSet.getLong(4)))
                        .withCreationDate(resultSet.getDate(5))
                        .withType(AccountType.valueOf(resultSet.getString(6)))
                        .withIban(resultSet.getLong(7))
                        .build();
                logger.info("Account found by id: " + account);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("While finding the account by id, the following exception appeared " + e);
        }
        return account;
    }

    @Override
    public List<Account> findByClient(Client client) {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "SELECT * FROM bank.account WHERE user_id=?";
        accounts = null;

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setLong(1, client.getId());
            resultSet = preparedStatement.executeQuery();
            accounts = client.getAccounts();
            while (resultSet.next()) {
                account = new AccountBuilder()
                        .withId(resultSet.getLong(1))
                        .withBalance(resultSet.getDouble(2))
                        .withActive(resultSet.getBoolean(3))
                        .withClient(clientRep.findById(resultSet.getLong(4)))
                        .withCreationDate(resultSet.getDate(5))
                        .withType(AccountType.valueOf(resultSet.getString(6)))
                        .withIban(resultSet.getLong(7))
                        .build();
                logger.info("Account found by user id: " + account);
                accounts.add(account);
            }
            resultSet.close();
            client.setAccounts(accounts);
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("While finding the accounts by user, the following exception appeared " + e);
        }

        return accounts;
    }

    @Override
    public List<Account> findAll() {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "SELECT * FROM bank.client";
        accounts = new ArrayList<>();

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                account = new AccountBuilder()
                        .withId(resultSet.getLong(1))
                        .withBalance(resultSet.getDouble(2))
                        .withActive(resultSet.getBoolean(3))
                        .withClient(clientRep.findById(resultSet.getLong(4)))
                        .withCreationDate(resultSet.getDate(5))
                        .withType(AccountType.valueOf(resultSet.getString(6)))
                        .withIban(resultSet.getLong(7))
                        .build();

                logger.info("Account found by id: " + account);
                accounts.add(account);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("Exception while finding all users" + e);
        }
        return accounts;
    }

    @Override
    public Account update(Account account) {
        sqlQuery = "UPDATE bank.account SET balance=?, active=?, user_id=?, creation_date=?, type=? WHERE id=?";
        int changedRows;
        connection = jdbConnectionWrapper.getConnection();

        try {
            preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setDouble(1, account.getBalance());
            preparedStatement.setBoolean(2, account.isActive());
            preparedStatement.setLong(3, account.getClient().getId());
            preparedStatement.setDate(4,account.getCreationDate());
            preparedStatement.setString(5,account.getType().name());
            preparedStatement.setLong(6, account.getId());

            changedRows = preparedStatement.executeUpdate();
            if (changedRows > 0) {
                logger.info("Just updated the account " + account);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("While updating account, the following exception appeared" + e);
        }
        return account;
    }

    @Override
    public boolean delete(Long id) {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "DELETE FROM bank.account WHERE id = ?;";
        Boolean result = false;

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setLong(1, id);

            preparedStatement.executeUpdate();
            logger.info("Deleted the account with the id: " + id);
            result = true;
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("Exception while deleting account with id: " + id + e);
        }

        return result;
    }

    @Override
    public int countAccounts() {
        sqlQuery = "SELECT COUNT(id) FROM account;";
        int countClients = 0;

        connection = jdbConnectionWrapper.getConnection();
        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next())
                countClients = resultSet.getInt(1);

            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("While counting the no of accounts, the following exception appeared" + e);
        }
        return countClients;
    }

    @Override
    public int countUserAccounts(Client client) {
        sqlQuery = "SELECT COUNT(id) FROM account WHERE user_id=?;";
        int countClients = 0;

        connection = jdbConnectionWrapper.getConnection();
        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setLong(1, client.getId());

            resultSet = preparedStatement.executeQuery();
            if(resultSet.next())
                countClients = resultSet.getInt(1);

            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("While counting the no of accounts, the following exception appeared" + e);
        }
        return countClients;
    }
}

package repository.impl;

import model.Client;
import repository.ReportRep;

import java.sql.*;

public class ReportRepImpl implements ReportRep {
    private final JDBConnectionWrapper jdbConnectionWrapper;
    private Connection connection;
    private String sqlQuery;
    private ResultSet resultSet;
    private PreparedStatement preparedStatement;

    public ReportRepImpl(JDBConnectionWrapper jdbConnectionWrapper) {
        this.jdbConnectionWrapper = jdbConnectionWrapper;
    }


    @Override
    public void insertReport(Long userId, String report, Date date) {
        sqlQuery = "INSERT INTO bank.user_report (user_id, report, date) " +
                "VALUES (?, ?, ?)";

        connection = jdbConnectionWrapper.getConnection();
        try{
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setLong(1, userId);
            preparedStatement.setString(2, report);
            preparedStatement.setDate(3, date);

            preparedStatement.executeUpdate();

        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String makeReport(Long userId, Date date1, Date date2) {
        String reports = "";
        String report = "";

        sqlQuery = "SELECT * FROM bank.user_report WHERE user_id = ? AND date >= ? AND date <= ?";

        connection = jdbConnectionWrapper.getConnection();
        try{
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setLong(1, userId);
            preparedStatement.setDate(2, date1);
            preparedStatement.setDate(3, date2);

            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                report = resultSet.getLong(1) +
                         resultSet.getString(3) +
                         resultSet.getDate(4);
                reports = reports + report + "\n";
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }

        return reports;
    }
}

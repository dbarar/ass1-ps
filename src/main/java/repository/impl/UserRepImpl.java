package repository.impl;

import model.User;
import model.builder.UserBuilder;
import model.enumeration.UserRole;
import repository.UserRep;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class UserRepImpl implements UserRep{

    private final JDBConnectionWrapper jdbConnectionWrapper;
    private final Logger logger;
    private Connection connection;
    private String sqlQuery;
    private ResultSet resultSet;
    private PreparedStatement preparedStatement;
    private User user;
    private List<User> users;

    public UserRepImpl(JDBConnectionWrapper jdbConnectionWrapper, Logger logger) {
        this.jdbConnectionWrapper = jdbConnectionWrapper;
        this.logger = logger;
    }

    @Override
    public User create(UserRole role, String username, String password, String name) {
        sqlQuery = "INSERT INTO bank.user (role, username, password, name) VALUES (?, ?, ?, ?);";
        user = new UserBuilder().withUserRole(role)
                        .withUsername(username)
                        .withPassword(password)
                        .withName(name)
                        .build();
        connection = jdbConnectionWrapper.getConnection();
        try {
            preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, role.name());
            preparedStatement.setString(2, username);
            preparedStatement.setString(3, password);
            preparedStatement.setString(4, name);

            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next())
                user.setId(resultSet.getLong(1));

            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("While creating user, the following exception appeared" + e);
            return null;
        }
        logger.info("Created the user" + user);
        return user;
    }

    @Override
    public User findById(Long id) {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "SELECT * FROM bank.user WHERE id=?";

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                user = new UserBuilder()
                        .withId(resultSet.getLong(1))
                        .withUserRole(UserRole.valueOf(resultSet.getString(2)))
                        .withUsername(resultSet.getString(3))
                        .withPassword(resultSet.getString(4))
                        .withName(resultSet.getString(5))
                        .build();
                logger.info("User found by id: " + user);
                return user;
            }

            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("While finding the user, the following exception appeared " + e);
        }

        return null;
    }

    @Override
    public List<User> findAll() {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "SELECT * FROM bank.user";
        users = new ArrayList<User>();

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user = new UserBuilder()
                        .withId(resultSet.getLong(1))
                        .withUserRole(UserRole.valueOf(resultSet.getString(2)))
                        .withUsername(resultSet.getString(3))
                        .withPassword(resultSet.getString(4))
                        .withName(resultSet.getString(5))
                        .build();

                logger.info("User found by id: " + user);
                users.add(user);
            }
            resultSet.close();

            return users;
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("Exception while finding all users" + e);
        }
        return null;
    }

    @Override
    public User update(User user) {
        sqlQuery = "UPDATE bank.user SET role=?, username=?, password=?, name=? WHERE id=?;";
        int changedRows;
        connection = jdbConnectionWrapper.getConnection();

        try {
            preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, user.getRole().name());
            preparedStatement.setString(2, user.getUsername());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setString(4, user.getName());
            preparedStatement.setLong(5, user.getId());

            changedRows = preparedStatement.executeUpdate();
            if (changedRows > 0){
                logger.info("Just updated the user: " + user);
                return user;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("While updating user, the following exception appeared" + e);
        }
        return null;
    }

    @Override
    public boolean delete(Long id) {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "DELETE FROM bank.user WHERE id = ?;";

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setLong(1, id);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("Exception while deleting user with id: " + id + e);
            return false;
        }

        logger.info("Deleted the user with the id: " + id);
        return true;
    }

    @Override
    public int countUsers() {
        sqlQuery = "SELECT COUNT(id) FROM user;";
        int countUsers = 0;

        connection = jdbConnectionWrapper.getConnection();
        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next())
                countUsers = resultSet.getInt(1);

            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("While counting the no of users, the following exception appeared" + e);
        }
        return countUsers;
    }

    @Override
    public User loadByUserName(String userName) {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "SELECT * FROM bank.user WHERE username=?";

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setString(1, userName);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                user = new UserBuilder()
                        .withId(resultSet.getLong(1))
                        .withUserRole(UserRole.valueOf(resultSet.getString(2)))
                        .withUsername(resultSet.getString(3))
                        .withPassword(resultSet.getString(4))
                        .withName(resultSet.getString(5))
                        .build();
                logger.info("User loaded by username: " + user);
                return user;
            }

            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("While loading the user by username, the following exception appeared " + e);
        }

        return null;
    }
}

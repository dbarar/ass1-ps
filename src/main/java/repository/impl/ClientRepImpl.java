package repository.impl;

import model.Account;
import model.Client;
import model.builder.ClientBuilder;
import repository.ClientRep;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class ClientRepImpl implements ClientRep {
    private final JDBConnectionWrapper jdbConnectionWrapper;

    private final Logger logger;
    private Connection connection;
    private String sqlQuery;
    private ResultSet resultSet;
    private PreparedStatement preparedStatement;
    private Client client;
    private List<Client> clients;

    public ClientRepImpl(JDBConnectionWrapper jdbConnectionWrapper, Logger logger) {
        this.jdbConnectionWrapper = jdbConnectionWrapper;
        this.logger = logger;
    }

    @Override
    public Client create(String name, Long personalNumericalCode, int identityCardNumber, String address) {
        sqlQuery = "INSERT INTO bank.client (name, personal_numerical_code, identity_card_number, address) " +
                "VALUES (?, ?, ?, ?)";
        client = new ClientBuilder()
                .withName(name)
                .withPersonalNumericalCode(personalNumericalCode)
                .withIdentityCardNumber(identityCardNumber)
                .withAddress(address)
                .build();
        connection = jdbConnectionWrapper.getConnection();
        try{
            preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, name);
            preparedStatement.setLong(2, personalNumericalCode);
            preparedStatement.setInt(3, identityCardNumber);
            preparedStatement.setString(4,address);

            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();

            if(resultSet.next())
                client.setId(resultSet.getLong(1));

            resultSet.close();
        }catch (SQLException e){
            e.printStackTrace();
            logger.info("While creating client, the following exception appeared" + e);
            return null;
        }
        logger.info("Created client:" + client);

        return client;
    }

    @Override
    public Client findByCNP(Long cnp) {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "SELECT * FROM bank.client WHERE personal_numerical_code=?";

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setLong(1, cnp);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                client = new ClientBuilder()
                        .withId(resultSet.getLong(1))
                        .withName(resultSet.getString(2))
                        .withPersonalNumericalCode(resultSet.getLong(3))
                        .withIdentityCardNumber(resultSet.getInt(4))
                        .withAddress(resultSet.getString(5))
                        .build();
                logger.info("Client found by cnp: " + client);
                return client;
            }

            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("While finding the client by cnp, the following exception appeared " + e);
        }

        return null;
    }

    @Override
    public Client findById(Long id) {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "SELECT * FROM bank.client WHERE id=?";

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                client = new ClientBuilder()
                        .withId(resultSet.getLong(1))
                        .withName(resultSet.getString(2))
                        .withPersonalNumericalCode(resultSet.getLong(3))
                        .withIdentityCardNumber(resultSet.getInt(4))
                        .withAddress(resultSet.getString(5))
                        .build();
                logger.info("Client found by id: " + client);
                return client;
            }

            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("While finding the client by id, the following exception appeared " + e);
        }

        return null;
    }

    @Override
    public List<Client> findAll() {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "SELECT * FROM bank.client";
        clients = new ArrayList<Client>();

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                client = new ClientBuilder()
                        .withId(resultSet.getLong(1))
                        .withName(resultSet.getString(2))
                        .withPersonalNumericalCode(resultSet.getLong(3))
                        .withIdentityCardNumber(resultSet.getInt(4))
                        .withAddress(resultSet.getString(5))
                        .build();

                logger.info("Client found by id: " + client);
                clients.add(client);
            }
            resultSet.close();

            return clients;
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("Exception while finding all users" + e);
        }
        return null;
    }

    @Override
    public Client update(Client client) {
        sqlQuery = "UPDATE bank.client SET name=?, personal_numerical_code=?, identity_card_number=?, address=? WHERE id=?;";
        int changedRows;
        connection = jdbConnectionWrapper.getConnection();

        try {
            preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, client.getName());
            preparedStatement.setLong(2, client.getPersonalNumericalCode());
            preparedStatement.setInt(3, client.getIdentityCardNumber());
            preparedStatement.setString(4,client.getAddress());
            preparedStatement.setLong(5, client.getId());

            changedRows = preparedStatement.executeUpdate();
            if (changedRows > 0){
                logger.info("Just updated the client: " + client);
                return client;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("While updating client, the following exception appeared" + e);
        }
        return null;
    }

    @Override
    public boolean delete(Long id) {
        connection = jdbConnectionWrapper.getConnection();
        sqlQuery = "DELETE FROM bank.client WHERE id = ?;";

        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setLong(1, id);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("Exception while deleting client with id: " + id + e);
            return false;
        }

        logger.info("Deleted the client with the id: " + id);
        return true;
    }

    @Override
    public int countUsers() {
        sqlQuery = "SELECT COUNT(id) FROM client;";
        int countClients = 0;

        connection = jdbConnectionWrapper.getConnection();
        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next())
                countClients = resultSet.getInt(1);

            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("While counting the no of clients, the following exception appeared" + e);
        }
        return countClients;
    }
}

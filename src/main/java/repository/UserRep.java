package repository;

import model.User;
import model.enumeration.UserRole;

import java.util.List;

public interface UserRep {

    User create(UserRole role, String username, String password, String name);

    User findById(Long id);

    List<User> findAll();

    User update(User user);

    boolean delete(Long id);

    int countUsers();

    User loadByUserName(String userName);
}

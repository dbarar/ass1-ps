package repository;

import model.Client;

import java.sql.Date;


public interface ReportRep {

    void insertReport(Long userId, String report, Date date);

    String makeReport(Long userId, Date date1, Date date2);
}

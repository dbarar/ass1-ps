package repository;

import model.Account;
import model.Client;
import model.enumeration.AccountType;

import java.sql.Date;
import java.util.List;

public interface AccountRep {

    Account create(AccountType accountType, Double balance, Date creationDate, Boolean active, Client client, Long iban);

    Account findByIban(Long iban);

    Account findById(Long id);

    List<Account> findByClient(Client user);

    List<Account> findAll();

    Account update(Account account);

    boolean delete(Long id);

    int countAccounts();

    int countUserAccounts(Client client);
}

package view;

import Instances.MainInstances;
import constants.Consts;
import model.User;
import model.enumeration.UserRole;
import service.UserService;
import service.impl.UserServiceImpl;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UserLoginView{

    private static final String TITLE = "Login View";
    private JFrame jFrame;
    public JPanel panel1;
    private JLabel Username;
    private JLabel Password;
    private JTextField UsernameTextField;
    private JTextField PasswordTextField;
    private JButton LogInBut;

    private MainInstances main = MainInstances.getInstance();


    public UserLoginView(){
        jFrame = new JFrame("Login");
        LogInBut.addActionListener(new LoginActionListener());
    }

    private class LoginActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String username = getUsernameTextField().getText();
            String password = getPasswordTextField().getText();

            User user = MainInstances.getUserService().login(username, password);
            if (user == null)
                JOptionPane.showMessageDialog(jFrame,"Wrong username or password");
            else{
                if (user.getRole() == UserRole.USER){
                    JFrame frame = new JFrame("Regular user service");
                    frame.setContentPane(new RegularUserView().getMainPanel());
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame.pack();
                    frame.setVisible(true);
                }else{
                    JFrame frame = new JFrame("Admin user service");
                    frame.setContentPane(new AdminUserView().getMainPanel());
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame.pack();
                    frame.setVisible(true);
                }

            }
        }
    }

    public JPanel getPanel1() {
        return panel1;
    }

    public JTextField getUsernameTextField() {
        return UsernameTextField;
    }
    public JTextField getPasswordTextField() {
        return PasswordTextField;
    }

//    public static void main(String[] args){
//        JFrame frame = new JFrame("Bank application");
//        frame.setContentPane(new UserLoginView(userService).getPanel1());
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame.pack();
//        frame.setVisible(true);
//    }
}

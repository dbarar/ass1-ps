package view;

import Instances.MainInstances;
import model.Account;
import model.Client;
import model.enumeration.AccountType;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.List;

public class RegularUserView {

    private JFrame jFrame;
    private JPanel MainPanel;
    private JPanel ClientPanel;
    private JButton CLEARCLIENTFIELDSButton;
    private JButton CREATECLIENTButton;
    private JButton FINDCLIENTBYIDButton;
    private JButton FINDCLIENTBYCNPButton;
    private JButton UPDATECLIENTButton;
    private JButton DELETECLIENTButton;
    private JTextField IdTextField;
    private JTextField nameTextField;
    private JTextField CNPTextField;
    private JTextField IdentityCardNoTextField;
    private JTextField AddressTextField;
    private JLabel ID;
    private JLabel CNP;
    private JLabel Name;
    private JLabel IdentityCardNo;
    private JLabel Address;
    private JPanel AccountPanel;
    private JButton CLEARACCOUNTFIELDSButton;
    private JButton CREATEACCOUNTButton;
    private JButton FINDACCOUNTBYIDButton;
    private JButton FINDACCOUNTBYCLIENTButton;
    private JButton UPDATEACCOUNTButton;
    private JButton DELETEACCOUNTButton;
    private JButton FINDACCOUNTBYIBANButton;
    private JButton NEXTACCOUNTButton;
    private JButton PREVIOUSACCOUNTButton;
    private JTextField IDAccountTextField;
    private JTextField BalanceTextField;
    private JTextField ActiveTextField;
    private JTextField creationDateTextField;
    private JTextField typeTextField;
    private JTextField IBANTextField;
    private JPanel Transfer;
    private JButton FINDACCOUNTTOTRANSFERButton;
    private JButton TRANSFERMONEYButton;
    private JTextField IBAN2TextField;
    private JTextField balance2TextField;
    private JTextField active2TextField;
    private JTextField type2TextField1;
    private JTextField moneyToTransferTextField;
    private JButton LOGOUTButton;

    private Long accountTransferIban;
    private Double accountTransferBalance;
    private Boolean accountTranferActive;
    private AccountType accountTypeTransfer;
    private Double moneyToTranfer;

    private Long clientId;
    private String clientName;
    private Long clientCNP;
    private int clientIdentityCardNo;
    private String clientAddress;

    private Long accountId;
    private Double accountBalance;
    private Boolean accountActive;
    private Long accountUserId;
    private Date accountCreationDate;
    private AccountType accountType;
    private Long accountIban;

    private List<Account> accounts;
    private int indexAccounts = 0;

    public RegularUserView() {
        CLEARCLIENTFIELDSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearClientFields();
            }
        });
        CREATECLIENTButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getClientFields();
                IdTextField.setText(MainInstances.getRegularUserService().createClient(clientName, clientCNP, clientIdentityCardNo, clientAddress).getId().toString());
            }
        });
        FINDCLIENTBYIDButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getClientFields();
                Client client = MainInstances.getRegularUserService().findClientById(clientId);
                if(client == null) {
                    JOptionPane.showMessageDialog(jFrame, "Client not found");
                    clearClientFields();
                }else{
                    setClientFields(client.getId(), client.getName(), client.getPersonalNumericalCode(), client.getIdentityCardNumber(), client.getAddress());
                }
            }
        });
        FINDCLIENTBYCNPButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getClientFields();
                Client client = MainInstances.getRegularUserService().findClientByCNP(clientCNP);
                if(client == null) {
                    JOptionPane.showMessageDialog(jFrame, "Client not found");
                    clearClientFields();
                }else{
                    setClientFields(client.getId(), client.getName(), client.getPersonalNumericalCode(), client.getIdentityCardNumber(), client.getAddress());
                }
            }
        });
        UPDATECLIENTButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getClientFields();
                MainInstances.getRegularUserService().updateClient(clientId, clientName, clientCNP, clientIdentityCardNo, clientAddress);
            }
        });
        DELETECLIENTButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getClientFields();

                if(MainInstances.getRegularUserService().deleteClient(clientId))
                    JOptionPane.showMessageDialog(jFrame,"Client deleted");
                else
                    JOptionPane.showMessageDialog(jFrame,"Client not found");

                clearClientFields();
            }
        });
        CLEARACCOUNTFIELDSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearAccountFields();
            }
        });
        CREATEACCOUNTButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getClientFields();
                getAccountFields();

                Account account = MainInstances.getRegularUserService().createAccount(accountType, accountBalance, accountCreationDate, accountActive, clientId, accountIban);

                setAccountFields(account.getId(), account.getBalance(), account.isActive(), account.getCreationDate(), account.getType(), account.getIban());
            }
        });
        FINDACCOUNTBYIBANButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getAccountFields();

                Account account = MainInstances.getRegularUserService().findAccountByIban(accountIban);

                if(account == null) {
                    JOptionPane.showMessageDialog(jFrame, "Account not found");
                    clearClientFields();
                    clearAccountFields();
                }else{
                    Client client = account.getClient();
                    setClientFields(client.getId(), client.getName(), client.getPersonalNumericalCode(), client.getIdentityCardNumber(), client.getAddress());
                    setAccountFields(account.getId(), account.getBalance(), account.isActive(), account.getCreationDate(), account.getType(), account.getIban());
                }
            }
        });
        FINDACCOUNTBYIDButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getAccountFields();

                Account account = MainInstances.getRegularUserService().findAccountById(accountId);

                if(account == null) {
                    JOptionPane.showMessageDialog(jFrame, "Account not found");
                    clearClientFields();
                    clearAccountFields();
                }else{
                    Client client = account.getClient();
                    setClientFields(client.getId(), client.getName(), client.getPersonalNumericalCode(), client.getIdentityCardNumber(), client.getAddress());
                    setAccountFields(account.getId(), account.getBalance(), account.isActive(), account.getCreationDate(), account.getType(), account.getIban());
                }
            }
        });
        FINDACCOUNTBYCLIENTButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getClientFields();
                getAccountFields();
                accounts = MainInstances.getRegularUserService().findAccountsByClient(clientId);
                indexAccounts = 0;
                displayOneAccountFromAccountsList();
            }
        });
        PREVIOUSACCOUNTButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                indexAccounts--;
                displayOneAccountFromAccountsList();
            }
        });
        NEXTACCOUNTButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                indexAccounts++;
                displayOneAccountFromAccountsList();
            }
        });
        UPDATEACCOUNTButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getClientFields();
                getAccountFields();
                MainInstances.getRegularUserService().updateAccount(accountId, accountType, accountBalance, accountCreationDate, accountActive, clientId, accountIban);
            }
        });
        DELETEACCOUNTButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getAccountFields();

                if(MainInstances.getAdminUserService().deleteUser(accountId))
                    JOptionPane.showMessageDialog(jFrame,"Account deleted");
                else
                    JOptionPane.showMessageDialog(jFrame,"Account not found");

                clearAccountFields();
            }
        });
        FINDACCOUNTTOTRANSFERButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getAccountTransferFields();

                Account account = MainInstances.getRegularUserService().findAccountByIban(accountTransferIban);

                if(account == null) {
                    JOptionPane.showMessageDialog(jFrame, "Account to transfer not found");
                }else{
                    setAccountTransferFields(account.getIban(), account.getBalance(), account.isActive(), account.getType(), 0.0);
                }
            }
        });
        TRANSFERMONEYButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getAccountFields();
                getAccountTransferFields();

                Account account = MainInstances.getRegularUserService().findAccountByIban(accountIban);
                Account accountTransfer = MainInstances.getRegularUserService().findAccountByIban(accountTransferIban);

                MainInstances.getRegularUserService().transfer(account, accountTransfer, moneyToTranfer);

                setAccountFields(account.getId(), account.getBalance(), account.isActive(), account.getCreationDate(), account.getType(), account.getIban());
                setAccountTransferFields(accountTransfer.getIban(), accountTransfer.getBalance(), accountTransfer.isActive(), accountTransfer.getType(), 0.0);
            }
        });
        LOGOUTButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MainInstances.getContextHolder().logout();
                JFrame frame = new JFrame("Bank application");
                frame.setContentPane(new UserLoginView().getPanel1());
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.pack();
                frame.setVisible(true);
            }
        });
    }

    public void clearClientFields(){
        IdTextField.setText("");
        nameTextField.setText("");
        CNPTextField.setText("");
        IdentityCardNoTextField.setText("");
        AddressTextField.setText("");
    }

    public void setClientFields(Long id, String name, Long CNP, int clientIdentityCardNo, String address){
        IdTextField.setText(id.toString());
        nameTextField.setText(name);
        CNPTextField.setText(CNP.toString());
        IdentityCardNoTextField.setText(clientIdentityCardNo + "");
        AddressTextField.setText(address);
    }

    public void getClientFields(){

        clientId = !IdTextField.getText().equals("") ? Long.parseLong(IdTextField.getText()) : 0L;
        clientName = nameTextField.getText();
        clientCNP = !CNPTextField.getText().equals("") ? Long.parseLong(CNPTextField.getText()) : 0L;
        clientIdentityCardNo = !IdentityCardNoTextField.getText().equals("") ? Integer.parseInt(IdentityCardNoTextField.getText()) : 0;
        clientAddress = AddressTextField.getText();
    }

    public void clearAccountFields(){
        IDAccountTextField.setText("");
        BalanceTextField.setText("");
        ActiveTextField.setText("");
        creationDateTextField.setText("");
        typeTextField.setText("");
        IBANTextField.setText("");
    }

    public void setAccountFields(Long id, Double balance, Boolean active, Date date, AccountType accountType, Long iban){
        IDAccountTextField.setText(id.toString());
        BalanceTextField.setText(balance.toString());
        ActiveTextField.setText(active.toString());
        creationDateTextField.setText(date.toString());
        typeTextField.setText(accountType.name());
        IBANTextField.setText(iban.toString());
    }

    public void getAccountFields(){
        accountId = !IDAccountTextField.getText().equals("") ? Long.parseLong(IDAccountTextField.getText()) : 0L;
        accountBalance = !BalanceTextField.getText().equals("") ? Double.parseDouble(BalanceTextField.getText()) : 0;
        accountActive = !ActiveTextField.getText().equals("") && Boolean.parseBoolean(ActiveTextField.getText());
        accountUserId = !IdTextField.getText().equals("") ? Long.parseLong(IdTextField.getText()) : 0L;
        accountCreationDate = !creationDateTextField.getText().equals("") ? Date.valueOf(creationDateTextField.getText()) : new Date(0000, 00, 00);
        accountType = !typeTextField.getText().equals("") ? AccountType.valueOf(typeTextField.getText()) : AccountType.CREDIT;
        accountIban = !IBANTextField.getText().equals("") ? Long.parseLong(IBANTextField.getText()) : 0L;
    }

    public void clearAccountTransferFields(){
        IBAN2TextField.setText("");
        balance2TextField.setText("");
        active2TextField.setText("");
        type2TextField1.setText("");
        moneyToTransferTextField.setText("");
    }

    public void setAccountTransferFields(Long accountTransferIban, Double accountTransferBalance, Boolean accountTranferActive, AccountType accountTypeTransfer, Double moneyToTranfer){
        IBAN2TextField.setText(accountTransferIban.toString());
        balance2TextField.setText(accountTransferBalance.toString());
        active2TextField.setText(accountTranferActive.toString());
        type2TextField1.setText(accountTypeTransfer.toString());
        moneyToTransferTextField.setText(moneyToTranfer.toString());
    }

    public void getAccountTransferFields(){
        accountTransferIban = !IBAN2TextField.getText().equals("") ? Long.parseLong(IBAN2TextField.getText()) : 0L;
        accountTransferBalance = !balance2TextField.getText().equals("") ? Double.parseDouble(balance2TextField.getText()) : 0;
        accountTranferActive = !active2TextField.getText().equals("") && Boolean.parseBoolean(active2TextField.getText());
        accountTypeTransfer = !type2TextField1.getText().equals("") ? AccountType.valueOf(typeTextField.getText()) : AccountType.CREDIT;
        moneyToTranfer = !moneyToTransferTextField.getText().equals("") ? Double.parseDouble(moneyToTransferTextField.getText()) : 0;
    }

    public JPanel getMainPanel() {
        return MainPanel;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    private void displayOneAccountFromAccountsList(){
        int accountsCount = accounts.size();
        if (indexAccounts >= accountsCount)
            indexAccounts = accountsCount - 1;
        if (indexAccounts < 0)
            indexAccounts = 0;
        Account account = accounts.get(indexAccounts);
        setAccountFields(account.getId(), account.getBalance(), account.isActive(), account.getCreationDate(), account.getType(), account.getIban());
    }
}

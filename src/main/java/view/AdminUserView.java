package view;

import Instances.MainInstances;
import model.User;
import model.enumeration.UserRole;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;

public class AdminUserView {
    private JFrame jFrame;
    private JPanel MainPanel;

    private JButton createUserButton;
    private JButton findUserByUserNameButton;
    private JButton findUserByIDButton;
    private JButton updateUserButton;
    private JButton deleteUserButton;
    private JTextField userNameTextField;
    private JTextField passwordTextField;
    private JTextField nameTextField;
    private JTextField roleTextField;
    private JTextField IDTextField;
    private JButton clearFieldsButton;
    private JButton makeReportButton;
    private JTextField fromTextField;
    private JTextField toTextField;
    private JTextArea reportTextArea;

    private Long id;
    private String username;
    private String password;
    private UserRole userRole;
    private String name;

    public AdminUserView() {

        createUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                username = userNameTextField.getText();
                password = passwordTextField.getText();
                userRole = UserRole.valueOf(roleTextField.getText());
                name = nameTextField.getText();

                IDTextField.setText(MainInstances.getAdminUserService().createUser(userRole, username, password, name).getId().toString());
            }
        });
        findUserByUserNameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                username = userNameTextField.getText();

                User user = MainInstances.getAdminUserService().findUserByUsername(username);
                if(user == null) {
                    JOptionPane.showMessageDialog(jFrame, "User not found");
                    clearFields();
                }else{
                    setFields(user.getId(), user.getUsername(), user.getPassword(), user.getRole(), user.getName());
                }
            }
        });
        findUserByIDButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                id = Long.parseLong(IDTextField.getText());

                User user = MainInstances.getAdminUserService().findUserById(id);
                if(user == null) {
                    JOptionPane.showMessageDialog(jFrame, "User not found");
                    clearFields();
                }else{
                    setFields(user.getId(), user.getUsername(), user.getPassword(), user.getRole(), user.getName());
                }
            }
        });
        updateUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                id = Long.parseLong(IDTextField.getText());
                username = userNameTextField.getText();
                password = passwordTextField.getText();
                userRole = UserRole.valueOf(roleTextField.getText());
                name = nameTextField.getText();

                MainInstances.getAdminUserService().updateUser(id, userRole, username, password, name);
            }
        });
        deleteUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                id = Long.parseLong(IDTextField.getText());

                if(MainInstances.getAdminUserService().deleteUser(id))
                    JOptionPane.showMessageDialog(jFrame,"User deleted");
                else
                    JOptionPane.showMessageDialog(jFrame,"User not found");

                clearFields();
            }
        });
        clearFieldsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               clearFields();
            }
        });
        makeReportButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Date date1 = Date.valueOf(fromTextField.getText());
                Date date2 = Date.valueOf(toTextField.getText());
                Long userId = Long.parseLong(IDTextField.getText());
                reportTextArea.setText(MainInstances.getReportRep().makeReport(userId, date1, date2));
            }
        });
    }

    public JPanel getMainPanel() {
        return MainPanel;
    }

    public void clearFields(){
        IDTextField.setText("");
        userNameTextField.setText("");
        passwordTextField.setText("");
        roleTextField.setText("");
        nameTextField.setText("");
    }

    public void setFields(Long id, String username, String password, UserRole userRole, String name){
        IDTextField.setText(id.toString());
        userNameTextField.setText(username);
        passwordTextField.setText(password);
        roleTextField.setText(userRole.name());
        nameTextField.setText(name);
    }
}

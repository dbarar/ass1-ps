package view;

import Instances.MainInstances;
import constants.Consts;
import repository.AccountRep;
import repository.ClientRep;
import repository.UserRep;
import repository.impl.AccountRepImpl;
import repository.impl.ClientRepImpl;
import repository.impl.JDBConnectionWrapper;
import repository.impl.UserRepImpl;
import service.AdminUserService;
import service.ContextHolder;
import service.RegularUserService;
import service.UserService;
import service.impl.AdminUserServiceImpl;
import service.impl.ContextHolderImpl;
import service.impl.RegularUserServiceImpl;
import service.impl.UserServiceImpl;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StartupView {
    private JButton goToLoginButton;
    private JPanel MainPanel;

    private MainInstances main = MainInstances.getInstance();


    public StartupView() {
        goToLoginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame frame = new JFrame("Bank application");
                frame.setContentPane(new UserLoginView().getPanel1());
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.pack();
                frame.setVisible(true);
            }
        });
    }

    public static void main(String[] args){
        JFrame frame = new JFrame("Bank application");
        frame.setContentPane(new StartupView().getMainPanel());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public JPanel getMainPanel() {
        return MainPanel;
    }
}

package Instances;

import constants.Consts;
import repository.AccountRep;
import repository.ClientRep;
import repository.ReportRep;
import repository.UserRep;
import repository.impl.*;
import service.*;
import service.impl.*;

public class MainInstances {
    private static MainInstances instance = null;

    //init db connection
    private static JDBConnectionWrapper jdbConnectionWrapper = new JDBConnectionWrapper(Consts.SCHEMA_NAME);

    //init repositories
    private static UserRep userRep = new UserRepImpl(jdbConnectionWrapper, Consts.logger);
    private static ClientRep clientRep = new ClientRepImpl(jdbConnectionWrapper, Consts.logger);
    private static AccountRep accountRep = new AccountRepImpl(jdbConnectionWrapper, Consts.logger, clientRep);

    //init services

    private static ReportRep reportRep = new ReportRepImpl(jdbConnectionWrapper);
    private static ContextHolder contextHolder = new ContextHolderImpl();
    private static UserService userService = new UserServiceImpl(userRep, contextHolder, Consts.logger);
    private static AdminUserService adminUserService = new AdminUserServiceImpl(userRep, contextHolder, Consts.logger);
    private static RegularUserService regularUserService = new RegularUserServiceImpl(userRep, contextHolder, Consts.logger, accountRep, clientRep, reportRep);

    public static JDBConnectionWrapper getJdbConnectionWrapper() {
        return jdbConnectionWrapper;
    }

    public static ReportRep getReportRep() {
        return reportRep;
    }

    public static UserRep getUserRep() {
        return userRep;
    }

    public static ClientRep getClientRep() {
        return clientRep;
    }

    public static AccountRep getAccountRep() {
        return accountRep;
    }

    public static ContextHolder getContextHolder() {
        return contextHolder;
    }

    public static UserService getUserService() {
        return userService;
    }

    public static AdminUserService getAdminUserService() {
        return adminUserService;
    }

    public static RegularUserService getRegularUserService() {
        return regularUserService;
    }


    private MainInstances() {

   }

   public static MainInstances getInstance(){
        if (instance == null){
            instance = new MainInstances();
        }
        return instance;
   }
}
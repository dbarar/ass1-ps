package repositoryTest;

import constants.Consts;
import model.Client;
import model.builder.ClientBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import repository.impl.ClientRepImpl;
import repository.impl.JDBConnectionWrapper;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ClientTest {

    private ClientRepImpl clientRepImpl;

    @Before
    public void setUp(){
        JDBConnectionWrapper jdbConnectionWrapper = new JDBConnectionWrapper(Consts.SCHEMA_NAME);
        clientRepImpl = new ClientRepImpl(jdbConnectionWrapper, Consts.logger);
    }

    @Test
    public void createUser(){
        Client user = clientRepImpl.create("Client_dummy", 2960901303910L, 602794, "dummy address");
        Client userFound = clientRepImpl.findById(user.getId());
        assertEquals(user, userFound);
        clientRepImpl.delete(user.getId());
    }

    @Test
    public void findByIdUser(){
        Client user = clientRepImpl.create("Client_dummy", 2960901303910L, 602794, "dummy address");
        Client userFound = clientRepImpl.findById(user.getId());
        assertEquals(user, userFound);
        clientRepImpl.delete(user.getId());
    }

    @Test
    public void findAllUsers(){
        List<Client> users = clientRepImpl.findAll();
        int countUsers = clientRepImpl.countUsers();
        assertEquals(users.size(), countUsers);
    }

    @Test
    public void updateUser(){
        Client user = clientRepImpl.create("Client_dummy", 2960901303910L, 602794, "dummy address");
        Client userToUpdate = new ClientBuilder()
                .withId(user.getId())
                .withName("changedName")
                .withPersonalNumericalCode(1960901303910L)
                .withIdentityCardNumber(123456)
                .withAddress("aaa")
                .build();
        clientRepImpl.update(userToUpdate);
        Client userUpdated = clientRepImpl.findById(user.getId());
        assertEquals(userToUpdate, userUpdated);
        clientRepImpl.delete(user.getId());
    }

    @Test
    public void deleteUser(){
        Client user = clientRepImpl.create("Client_dummy", 2960901303910L, 602794, "dummy address");
        Long id = user.getId();
        clientRepImpl.delete(id);
        assertNull(clientRepImpl.findById(id));
    }
    @After
    public void tearDown(){

    }
}

package repositoryTest;

import constants.Consts;
import model.User;
import model.builder.UserBuilder;
import model.enumeration.UserRole;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import repository.impl.JDBConnectionWrapper;
import repository.impl.UserRepImpl;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class UserTest {

    private UserRepImpl userRepImpl;

    @Before
    public void setUp(){
        JDBConnectionWrapper jdbConnectionWrapper = new JDBConnectionWrapper(Consts.SCHEMA_NAME);
        userRepImpl = new UserRepImpl(jdbConnectionWrapper, Consts.logger);
    }

    @Test
    public void createUser(){
        User user = userRepImpl.create(UserRole.ADMIN, "admin02_dummy", "pass", "grigorescu cuc");
        User userFound = userRepImpl.findById(user.getId());
        assertEquals(user, userFound);
        userRepImpl.delete(user.getId());
    }

    @Test
    public void findByIdUser(){
        User user = userRepImpl.create(UserRole.ADMIN, "admin02_dummy", "pass", "grigorescu cuc");
        User userFound = userRepImpl.findById(user.getId());
        assertEquals(user, userFound);
        userRepImpl.delete(user.getId());
    }

    @Test
    public void findAllUsers(){
        List<User> users = userRepImpl.findAll();
        int countUsers = userRepImpl.countUsers();
        assertEquals(users.size(), countUsers);
    }

    @Test
    public void updateUser(){
        User user = userRepImpl.create(UserRole.ADMIN, "admin02_dummy_update", "pass", "grigorescu cuc");
        User userToUpdate = new UserBuilder().withUserRole(UserRole.USER)
                .withId(user.getId())
                .withUsername("usser")
                .withPassword("pass")
                .withName("Floare Albastra")
                .build();
        userRepImpl.update(userToUpdate);
        User userUpdated = userRepImpl.findById(user.getId());
        assertEquals(userToUpdate, userUpdated);
        userRepImpl.delete(user.getId());
    }

    @Test
    public void deleteUser(){
        User user = userRepImpl.create(UserRole.ADMIN, "admin02_dummy_delete", "pass", "grigorescu cuc");
        Long id = user.getId();
        userRepImpl.delete(id);
        assertNull(userRepImpl.findById(id));
    }
    @After
    public void tearDown(){

    }
}

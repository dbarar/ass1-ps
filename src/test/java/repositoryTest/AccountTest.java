package repositoryTest;

import constants.Consts;
import model.Account;
import model.Client;
import model.builder.AccountBuilder;
import model.enumeration.AccountType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import repository.impl.AccountRepImpl;
import repository.impl.ClientRepImpl;
import repository.impl.JDBConnectionWrapper;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;

public class AccountTest {

    private AccountRepImpl accountRep;
    private ClientRepImpl clientRep;
    private Client client;
    private Account account;

    @Before
    public void setup(){
        JDBConnectionWrapper jdbConnectionWrapper = new JDBConnectionWrapper(Consts.SCHEMA_NAME);
        clientRep = new ClientRepImpl(jdbConnectionWrapper, Consts.logger);
        accountRep = new AccountRepImpl(jdbConnectionWrapper, Consts.logger, clientRep);
        client = clientRep.create("Client_dummy", 2960901303910L, 602794, "dummy address");
        account = accountRep.create(AccountType.CREDIT, 500.0, new Date(2018, 03, 23), true, client, 123456789123L);
    }

    @Test
    public void createAccount(){
        Account account = accountRep.create(AccountType.CREDIT, 500.0, new Date(2018, 03, 23), true, client, 1234567889L);
        assertEquals(account, accountRep.findById(account.getId()));
        accountRep.delete(account.getId());
    }

    @Test
    public void findById(){
        Account accountFound = accountRep.findById(account.getId());
        assertEquals(account, accountFound);
    }

    @Test
    public void update(){
        Account accountToUpdate = new AccountBuilder()
                .withId(account.getId())
                .withActive(true)
                .withBalance(60.0)
                .withClient(client)
                .withCreationDate(new Date(2018, 03, 24))
                .withType(AccountType.CREDIT)
                .build();
        accountRep.update(accountToUpdate);
        Account accountFound = accountRep.findById(account.getId());
        assertEquals(accountFound, accountToUpdate);
    }

    @Test
    public void delete(){
        Long id = account.getId();
        accountRep.delete(id);
        assertNull(accountRep.findById(id));
    }

    @After
    public void tearDown(){
        clientRep.delete(client.getId());
        accountRep.delete(account.getId());
    }
}
